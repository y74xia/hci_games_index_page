// js file

// positions is the list saving relative position of each button to the top left corner of background image and width/height, in percentage
const positions = [[8,81,12,10],[31,53,11,11],[12,66,9,11],[7,46,12,15],[24,37,12,12],[23,4,11,13],[56,27,10,13],[77,11,12,12],[80,29,14,16],[57,56,12,13]];
// n is the mount of links
const n = 10;

function onload(){
  // Set css of image, making it fit the screen
  setBackground();
  setButtons();
}

function setBackground(){
  // h,w indicates expect size of background image. Original value is set to original size of background image.
  let h = 842, w = 1200;
  let height = window.innerHeight*0.95;
  let width = window.innerWidth;
  $("div.container").height(height);
  $("div.container").width(width);
  height = height - parseInt($("h1.heading").css("marginTop")) - parseInt($("h1.heading").css("marginBottom")) - $("h1.heading").height();
  
  // Resize the background image according to room left on the screen
  let ratio = 1;
  if (h/height >  w/width) {
    ratio = h/height;
  } else {
    ratio = w/width;
  }
  h /= ratio; w /= ratio;
  $("div.backpic").height(h);
  $("div.backpic").width(w);
  $("div.backpic").css("background-size", w);
  $("div.backpic").css("marginLeft",(width-w)/2);
  $("div.backpic").css("marginRight",(width-w)/2);
}

function setButtons(){
  for (let i = 0; i < n; i++) {
    let class_name = ".button-" + (i+1).toString();
    $(class_name).css("left", positions[i][0].toString()+"%");
    $(class_name).css("top", positions[i][1].toString()+"%");
    $(class_name).css("width", positions[i][2].toString()+"%");
    $(class_name).css("height", positions[i][3].toString()+"%");
  }
}

// Helper function used to find button position. UNCOMMENT AND USE IN DEV ONLY
function recordLocation(){
  // let e = window.event;
  // let x = e.clientX;
  // let y = e.clientY;
  // console.log(x,y);
}